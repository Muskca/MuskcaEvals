/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.irit.muskcaevals;

import fr.irit.muskcaevals.Candidate.MuskcaCandidate;
import fr.irit.muskcaevals.Candidate.GoldCandidate;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;
import org.aksw.jena_sparql_api.core.QueryExecutionFactory;
import org.aksw.jena_sparql_api.delay.core.QueryExecutionFactoryDelay;
import org.aksw.jena_sparql_api.http.QueryExecutionFactoryHttp;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.rdf.model.RDFNode;

/**
 *
 * @author Fabien
 */
public class Evals {
    
    
    public static ArrayList<MuskcaCandidate> getAllMuskcaCands(String sparqlEndpoint){
        ArrayList<MuskcaCandidate> ret = new ArrayList<>();
        
        QueryExecutionFactory qef = new QueryExecutionFactoryHttp(sparqlEndpoint);
        qef = new QueryExecutionFactoryDelay(qef, 2000);
        String queryString = "PREFIX owl: <http://www.w3.org/2002/07/owl#>\n" +
                            "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
                            "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n" +
                            "\n" +
                            "SELECT ?subject (GROUP_CONCAT(?seeAlso;SEPARATOR=\"**\") AS ?allSeeAlsos) ?score ?scoreSimple ?scoreDegree \n" +
                            "WHERE {\n" +
                            "  ?subject rdf:type <http://muskca_system.fr/Triticum_6sources/NodeCandidate>.\n" +
                            "  ?subject <http://muskca_evals/wasDerivedFrom> ?derive."+
                            "  ?derive rdfs:seeAlso ?seeAlso."+
                            "  ?subject <http://muskca_system.fr/Triticum_6sources/hadTrustSimple> ?scoreSimple."+
                            "  ?subject <http://muskca_system.fr/Triticum_6sources/hadTrustDegree> ?scoreDegree."+
                            "  ?subject <http://muskca_system.fr/Triticum_6sources/hadTrustChoquet> ?score."+
                            "} GROUP BY ?subject ?score ?scoreSimple ?scoreDegree";
        QueryExecution qe = qef.createQueryExecution(queryString);

        
        
        ResultSet rs = qe.execSelect();
        int nbSeeAlsos = 0;
        MuskcaCandidate minCand = null;
        int min = 10;
        MuskcaCandidate maxCand =null;
        int max = 0;
        while(rs.hasNext()){
            QuerySolution sol = rs.next();
            RDFNode candNode = sol.get("subject");
            String candUri = candNode.toString();
            String[] seeAlsos = sol.get("allSeeAlsos").toString().split("\\*\\*");
            MuskcaCandidate cand = new MuskcaCandidate(candUri, seeAlsos, sol.get("score").asLiteral().getDouble(), sol.get("scoreSimple").asLiteral().getDouble(), sol.get("scoreDegree").asLiteral().getDouble());
            nbSeeAlsos += seeAlsos.length;
            if(min > seeAlsos.length){
                min = seeAlsos.length;
                minCand = cand;
            }
            if(max < seeAlsos.length){
                max = seeAlsos.length;
                maxCand = cand;
            }
            ret.add(cand);
        }
        
        System.out.println("STATS : ");
        System.out.println("MIN : "+min);
        System.out.println("\t "+minCand.getSeeAlsos());
        System.out.println("MAX : "+max);
        System.out.println("\t "+maxCand.getSeeAlsos());
        System.out.println("MOYENNE : "+(nbSeeAlsos/ret.size()));
        
        return ret;
    }
    
    
    public static ArrayList<GoldCandidate> getAllGoldCands(){
        ArrayList<GoldCandidate> ret = new ArrayList<>();
        
        QueryExecutionFactory qef = new QueryExecutionFactoryHttp("http://localhost:3030/AgronomicTaxon_goldstandard");
        qef = new QueryExecutionFactoryDelay(qef, 2000);
        String queryString = "PREFIX owl: <http://www.w3.org/2002/07/owl#>\n" +
                            "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
                            "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n" +
                            "\n" +
                            "SELECT ?subject (GROUP_CONCAT(?seeAlso;SEPARATOR=\"**\") AS ?allSeeAlso)\n" +
                            "WHERE {\n" +
                            "  ?subject rdf:type owl:NamedIndividual.\n" +
                            "  ?subject rdfs:seeAlso ?seeAlso.\n" +
                            "} GROUP BY ?subject";
        QueryExecution qe = qef.createQueryExecution(queryString);
        
        ResultSet rs = qe.execSelect();
        int nbSeeAlsos = 0;
        GoldCandidate minCand = null;
        int min = 10;
        GoldCandidate maxCand =null;
        int max = 0;
        while(rs.hasNext()){
            QuerySolution sol = rs.next();
            RDFNode candNode = sol.get("subject");
            String candUri = candNode.toString();
            String[] seeAlsos = sol.get("allSeeAlso").toString().split("\\*\\*");
            GoldCandidate cand = new GoldCandidate(candUri, seeAlsos);
            nbSeeAlsos += seeAlsos.length;
            if(min > seeAlsos.length){
                min = seeAlsos.length;
                minCand = cand;
            }
            if(max < seeAlsos.length){
                max = seeAlsos.length;
                maxCand = cand;
            }
            if(seeAlsos.length == 7){
                System.out.println(Arrays.asList(seeAlsos));
            }
            ret.add(cand);
        }
        
        System.out.println("STATS : ");
        System.out.println("MIN : "+min);
        System.out.println("\t "+minCand.getSeeAlsos());
        System.out.println("MAX : "+max);
        System.out.println("\t "+maxCand.getSeeAlsos());
        System.out.println("MOYENNE : "+(nbSeeAlsos/ret.size()));
        return ret;
    }
    
    
//    public static float computePrecision(ArrayList<MuskcaCandidate> muskcaCands, ArrayList<GoldCandidate> goldCands, float limit){
//        float ret = 0;
//        int nbCand = 0;
//        float nbValid = 0;
//        
//        for(MuskcaCandidate cand : muskcaCands){
//            if(cand.getScore() > limit){
//                nbCand ++;
//                boolean valid = false;
//                int i = 0;
//                ArrayList<Float> allScore = new ArrayList<>();
//                while(i < goldCands.size()){
//                    GoldCandidate gcand = goldCands.get(i);
//                    Float included = gcand.include(cand);
//                    if(included != null && included > 0.0f){
//                        //nbValid += included;
//                        allScore.add(included);
//                        valid = true;
//                    }
//                    i++;
//                }
//                if(valid && !allScore.isEmpty()){
//                    nbValid += Collections.max(allScore);
//                }
//            }
//        }
//        System.out.print("\tPrecision : "+nbValid+" / "+nbCand);
//        return nbValid/nbCand;
//    }
    
    
    public static float computeResult(ArrayList<MuskcaCandidate> muskcaCands, ArrayList<GoldCandidate> goldCands, float limit, boolean precision){
        float ret = 0;
        int nbCand = 0;
        boolean nbCandComputed = false;
        int nbCandGold = 0;
        float nbValid = 0;
        
        for(GoldCandidate gcand : goldCands){
//            System.out.println("---------------------------");
//            System.out.println("Start considering gcand : "+gcand);
//            System.out.println(gcand.getSeeAlsos());
            nbCandGold ++;
            boolean valid = false;
            int i = 0;
            ArrayList<Float> allScore = new ArrayList<>();
            while(i < muskcaCands.size()){
                MuskcaCandidate cand = muskcaCands.get(i);
//                System.out.println("---> compare with : "+cand+"("+cand.getScore()+")");
//                System.out.println(cand.getSeeAlsos());
                if(cand.getScore()> limit){
                    if(!nbCandComputed)
                        nbCand ++;
                    Float included = cand.include(gcand);
//                    System.out.println("RESUT : "+included);
                    if(included != null && included > 0.0f){
                        //nbValid += included;
                        allScore.add(included);
                        valid = true;
                        cand.setValid(included);
                    }
                }
                i++;
            }
            if(valid && !allScore.isEmpty()){
                nbValid += Collections.max(allScore);
            }
            nbCandComputed = true;
        }
        if(precision){
            System.out.print("\tPrecision : "+nbValid+" / "+nbCand);
            ret = nbValid/nbCand;
        }
        else{
            System.out.print("\tRecall : "+nbValid+" / "+nbCandGold);
            ret = nbValid/nbCandGold;
        }
        
        return ret;
    }
    
    
    
    public static void computeScores(ArrayList<MuskcaCandidate> muskcaCands, ArrayList<GoldCandidate> goldCands){
        double limitd = 0;
        for(int i = 0; i < 10; i++){
            float limit = (float) limitd;
            System.out.println("Computing score at "+limit);
            float precision = computeResult(muskcaCands, goldCands, limit, true);
            System.out.println(" = "+precision);
            float recall = computeResult(muskcaCands, goldCands, limit, false);
            System.out.println(" = "+recall);
            limitd += 0.1;
        }
    }
    
    
    public static void computeConcordances(ArrayList<MuskcaCandidate> muskcaCands){
        HashMap<String, String> allSources = new HashMap<>();
        allSources.put("NAL", "http://lod.nal.usda.gov/nalt");
        allSources.put("NCBI", "ncbi.nlm.nih.gov");
        allSources.put("CABI", "cabthesaurus");
        allSources.put("ITIS", "www.itis.gov");
        allSources.put("TaxRef", "inpn.mnhn.fr");
        allSources.put("Agrovoc", "agrovoc");
        String[] sourcesNames;
        sourcesNames = allSources.keySet().toArray(new String[allSources.keySet().size()]);
        
        HashMap<String, Integer> nbCandFromSource = new HashMap<>();
        HashMap<String, HashMap<String, Integer>> results = new HashMap<>();

        for(int i = 0; i < sourcesNames.length; i++){
            String source1 = sourcesNames[i];
            nbCandFromSource.put(source1, 0);
            HashMap<String, Integer> sourceMap = new HashMap<>();
            for(int j = 0; j < sourcesNames.length; j++){
                String source2 = sourcesNames[j];
                sourceMap.put(source2, 0);
            }
            results.put(source1, sourceMap);
        }
        
        
        for(MuskcaCandidate cand : muskcaCands){
            ArrayList<String> sourcesImp = new ArrayList<>();
            for(String uri : cand.getSeeAlsos()){
                boolean founded = false;
                int i = 0;
                while(!founded && i < sourcesNames.length){
                    String sourceName = sourcesNames[i];
                    String testSource = allSources.get(sourceName);
                    if(uri.contains(testSource)){
                        sourcesImp.add(sourceName);
                        nbCandFromSource.put(sourceName, nbCandFromSource.get(sourceName)+1);
                        founded = true;
                    }
                    i++;
                }
            }
            for(String source1 : sourcesImp){
                for(String source2 : sourcesImp){
                    if(!source1.equals(source2)){
                        HashMap<String, Integer> source1Map = results.get(source1);
                        source1Map.put(source2, source1Map.get(source2) + 1);
                        results.put(source1, source1Map);
                    }
                }
            }
        }
        System.out.println(results);
        for(Entry<String, HashMap<String, Integer>> e : results.entrySet()){
            System.out.println("Concordance with : "+e.getKey());
            HashMap<String, Integer> resSource = e.getValue();
            for(Entry<String, Integer> eS : resSource.entrySet()){
                System.out.println("\t "+eS.getKey());
                System.out.println("\t\t "+eS.getValue()+"/"+nbCandFromSource.get(e.getKey())+" = "+((float)eS.getValue()/nbCandFromSource.get(e.getKey())));
            }
        }
    }
    
    
    public static void main(String[] args){
//        System.out.println("Getting all muskca cands ... ");
//        ArrayList<MuskcaCandidate> muskcaCands = getAllMuskcaCands("http://localhost:3030/AgronomicTaxon_Muskca");
//        System.out.println("All muskca candidates fetched : "+muskcaCands.size()+" cands");
        
        System.out.println("Getting all gold cands ... ");
        ArrayList<GoldCandidate> goldCands = getAllGoldCands();
        System.out.println("All gold candidates fetched : "+goldCands.size()+" cands");

        //computeScores(muskcaCands, goldCands);
        
        System.out.println("Getting all muskca EXT  cands ... ");
        ArrayList<MuskcaCandidate> muskcaCandsExt = getAllMuskcaCands("http://localhost:3030/AgronomicTaxon_MuskcaExtInd");
        System.out.println("All muskca candidates fetched : "+muskcaCandsExt.size()+" cands");

        computeScores(muskcaCandsExt, goldCands);
        
        System.out.println("ALL MUSKCA CANDIDATES ------------------------");
        Collections.sort(muskcaCandsExt, (MuskcaCandidate a, MuskcaCandidate b) -> {
            int retComp = 0;
            if(a.getScore() < b.getScore()){
                retComp = 1;
            }
            else if(a.getScore() > b.getScore()){
                retComp = -1;
            }
            return retComp;
        });
        System.out.println("Extracted Muskca Candidate : "+muskcaCandsExt.size());
        muskcaCandsExt.forEach((mCand) -> {
            System.out.println(mCand);
        });
        
//        System.out.println("COMPUTE CONCORDANCE ---------------");
//        
//        computeConcordances(muskcaCands);
        
    }
    
}
