/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.irit.muskcaevals.Candidate;

import com.google.common.base.Splitter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Fabien
 */
public abstract class Candidate {
    String uri;
    ArrayList<String> seeAlsos;
    
    public Candidate(String uri, String[] seeAlsos){
        this.uri = uri;
        this.seeAlsos = new ArrayList<>();
        for(String s : seeAlsos){
            if(!s.isEmpty()){
                this.seeAlsos.add(s);
            }
        }
    }
    
   public String getUri(){
       return this.uri;
   }
    
    public ArrayList<String> getSeeAlsos(){
        return this.seeAlsos;
    }
    
    
    public boolean containsSeeAlso(String source, String id){
        boolean ret = false;
        
        int i = 0;
        while(!ret && i < this.seeAlsos.size()){
            String seeAlso = this.seeAlsos.get(i);
            ret = (seeAlso.contains(source) && seeAlso.contains(id));
            i ++;
        }
        
        return ret;
    }
    
    public Float include(Candidate cand){
        Float ret = null;
        
        
        boolean included = true;
        int i = 0;
        String id = "";
        String source = "";
        boolean atLeastOne = false;
        int nbSourceShared = 0;
        while(i < cand.getSeeAlsos().size()){
            String firstSeeAlso = cand.getSeeAlsos().get(i);
            if(firstSeeAlso.contains("ncbi.nlm.nih.gov")){
                String query = firstSeeAlso.split("\\?")[1];
                HashMap<String, String> map = new HashMap<>();
                for(String s : query.split("&")){
                    String[] param = s.split("=");
                    if(param[0].equals("id")){
                        id = param[1];
                        break;
                    }
                }
                source = "ncbi.nlm.nih.gov";
            }
            else if(firstSeeAlso.contains("http://lod.nal.usda.gov/nalt")){
                source = "http://lod.nal.usda.gov/nalt";
                id = firstSeeAlso.substring(firstSeeAlso.lastIndexOf("/"));
            }
            else if(firstSeeAlso.contains("agrovoc")){
                source = "agrovoc";
                id = firstSeeAlso.substring(firstSeeAlso.lastIndexOf("/"));
            }
            else if(firstSeeAlso.contains("cabthesaurus")){
                source ="cabthesaurus";
                String query = firstSeeAlso.split("\\?")[1];
                Map<String, String> map = Splitter.on('&').trimResults().withKeyValueSeparator("=").split(query);
                id = map.get("w");
            }
            else if(firstSeeAlso.contains("www.itis.gov")){
                source ="www.itis.gov";
                String query = firstSeeAlso.split("\\?")[1];
                if(query.contains("#")){
                    query = query.substring(0, query.lastIndexOf("#"));
                }
                Map<String, String> map = Splitter.on('&').trimResults().withKeyValueSeparator("=").split(query);
                id = map.get("search_value");
            }
            else if(firstSeeAlso.contains("inpn.mnhn.fr")){
                source = "inpn.mnhn.fr";
                id = firstSeeAlso.substring(firstSeeAlso.lastIndexOf("/"));
            }
            else{
                //System.out.println("WTF : "+firstSeeAlso);
            }
            i++;
            included = this.containsSeeAlso(source, id) ;
            if(included){
                atLeastOne = true;
                nbSourceShared++;
            }
        }
        
        
        if(atLeastOne){
            ret =  (new Integer(nbSourceShared).floatValue())/this.getSeeAlsos().size();
        }
        else{
            
        }
//        else if(atLeastOne){
//            System.out.println("----------------------------------------------------------------");
//            System.out.println("AT LEAST ONE SHARED : ");
//            System.out.println("FIRST CAND : "+cand);
//            System.out.println(cand.getSeeAlsos());
//            System.out.println("SECOND CAND : "+this);
//            System.out.println(this.getSeeAlsos());
//            System.out.println("----------------------------------------------------------------");
//        }
        
        return ret;
    }
}
