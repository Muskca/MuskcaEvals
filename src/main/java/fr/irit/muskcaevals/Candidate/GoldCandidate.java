/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.irit.muskcaevals.Candidate;

import fr.irit.muskcaevals.Candidate.Candidate;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author Fabien
 */
public class GoldCandidate extends Candidate{
    
    public GoldCandidate(String uri, String[] seeAlsos) {
        super(uri, seeAlsos);
    }
    
}
