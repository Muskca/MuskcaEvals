/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.irit.muskcaevals.Candidate;

import fr.irit.muskcaevals.Candidate.Candidate;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author Fabien
 */
public class MuskcaCandidate extends Candidate{

    double score;
    double scoreSimple;
    double scoreDegree;
    
    float included = 0.0f;
    
    public MuskcaCandidate(String uri, String[] derived, double score, double scoreSimple, double scoreDegree){
        super(uri, derived);
        this.score = score;
        this.scoreSimple = scoreSimple;
        this.scoreDegree = scoreDegree;
    }
    
    public double getScore(){
        return this.score;
    }
    
    public void setValid(float included){
        if(included > this.included)
            this.included = included;
    }
    
    public String toString(){
        String ret = "Muskca Candidate (Valid : "+this.included+") : "+this.getUri()+"\n";
        ret += "\t Choquet : "+this.score+" -- Simple : "+this.scoreSimple+" -- Degree : "+this.scoreDegree+"\n";
        ret += "\t See alsos : \n";
        for(String seeAlso : this.getSeeAlsos()){
            ret += "\t\t "+seeAlso+"\n";
        }
        return ret;
    }
}
